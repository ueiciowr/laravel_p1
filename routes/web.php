<?php

use App\Http\Controllers\CidadesController;
use App\Http\Controllers\PostosController;
use App\Http\Controllers\PrecosController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/* ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐ Relatórios ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐ */
Route::get('reports/precos', [PostosController::class, 'precos']);
Route::get('reports/postos', [CidadesController::class, 'postos']);

/* ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐ Middleware ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐ */
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/* ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐ CRUD ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐ */
Route::resource('cidades', CidadesController::class)->middleware('auth');
Route::resource('postos', PostosController::class)->middleware('auth');
Route::resource('precos', PrecosController::class)->middleware('auth');