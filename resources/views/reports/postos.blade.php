@extends('reports.index')

@section('title')
  Preços dos combustíveis
@endsection

@section('content')
<section class="form-group h-100 d-flex flex-column justify-content-center align-items-center">
  <form
    action="{{ url('/reports/postos') }}"
    class="form-group w-25 d-flex flex-column justify-content-center align-items-center"
    method="GET"
  >
    <label for="postos">Escolha o Posto</label>
    <select name="id" class="form-control">
      @foreach ($cidades as $cidade)
        <option value="{{ $cidade->id }}" class="form-control">{{ $cidade->nome }}</option>
      @endforeach
    </select>
    <button
      type="submit"
      class="btn btn-primary mt-3"
    >
      Pesquisar
    </button>
  </form>
  <br>
  @if ($postos != null)
    <h2>Postos nesta cidade</h2>
    
    <table class="table table-striped table-dark">
      <tr>
        <th>Id</th>
        <th>Nome</th>
        <th>Endereço</th>
        <th>Bairro</th>
        <th>Bandeira</th>
        <th>Razao Social</th>
        <th>CNPJ</th>
      </tr>
      @if ($postos != null)
        @foreach ($postos as $posto)
        <tr>
          <td>{{ $posto->id }}</td>
          <td>{{ $posto->nome }}</td>
          <td>{{ $posto->endereco }}</td>
          <td>{{ $posto->bairro }}</td>
          <td>{{ $posto->bandeira }}</td>
          <td>{{ $posto->razao_social }}</td>
          <td>{{ $posto->cnpj }}</td>
        @endforeach
      @endif
      </tr>
    </table>
  
  @endif
  <section class="d-flex mt-4 flex-column justify-content-center align-center">
    <h2 class="text-center">Cidades disponíveis</h2>
    <table class="table table-striped table-dark">
      <tr>
        <th>Id</th>
        <th>Nome</th>
        <th>UF</th>
        <th>CEP</th>
      </tr>
      @foreach ($cidades as $cidade)
        <tr>
          <td>{{ $cidade->id }}</td>
          <td>{{ $cidade->nome }}</td>
          <td>{{ $cidade->uf }}</td>
          <td>{{ $cidade->cep }}</td>
        </tr>
      @endforeach
    </table>
  </section>
</section>
@endsection
