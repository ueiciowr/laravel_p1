@extends('reports.index')

@section('title')
  Preços dos combustíveis
@endsection

@section('content')
<section class="form-group h-100 d-flex flex-column justify-content-center align-items-center">
  <form
    action="{{ url('/reports/precos') }}"
    class="form-group w-25 d-flex flex-column justify-content-center align-items-center"
  >
    <label for="postos">Escolha o Posto</label>
    <select name="id" class="form-control">
      @foreach ($postos as $posto)
        <option value="{{ $posto->id }}" class="form-control">{{ $posto->nome }}</option>
      @endforeach
    </select>
    <button
      type="submit"
      class="btn btn-primary mt-3"
    >
      Pesquisar
    </button>
  </form>
  <br>
  @if ($preco_postos != null)
    <h2>Preços do posto de combustível</h2>
    
    <table class="table table-striped table-dark">
      <tr>
        <th>Id</th>
        <th>Combustível</th>
        <th>Data de Coleta</th>
        <th>Preço de Venda</th>
      </tr>
      @foreach ($preco_postos as $preco_posto)
        <tr>
          <td>{{ $preco_posto->id }}</td>
          <td>{{ $preco_posto->tipo_combustivel }}</td>
          <td>{{ $preco_posto->data_coleta }}</td>
          <td>{{ $preco_posto->preco_venda }}</td>
        </tr>
      @endforeach
    </table>
  @endif
  <section class="d-flex mt-4 flex-column justify-content-center align-center">
    <h2 class="text-center"><strong>Postos disponíveis</strong></h2>
    <table class="table table-striped table-dark">
      <tr>
        <th>Id</th>
        <th>Nome</th>
        <th>Endereço</th>
        <th>Bairro</th>
        <th>Bandeira</th>
        <th>Razao Social</th>
        <th>CNPJ</th>
        <th>Cidade</th>
      </tr>
      @foreach ($postos as $posto)
      <tr>
        <td>{{ $posto->id }}</td>
        <td>{{ $posto->nome }}</td>
        <td>{{ $posto->endereco }}</td>
        <td>{{ $posto->bairro }}</td>
        <td>{{ $posto->bandeira }}</td>
        <td>{{ $posto->razao_social }}</td>
        <td>{{ $posto->cnpj }}</td>
        <td>{{ $posto->cidades->nome }}</td>
      @endforeach
      </tr>
    </table>
  </section>
</section>
@endsection
