<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400;600;900&display=swap" rel="stylesheet">
  <title>@yield('title')</title>
</head>
<style>
  th, td, h1, h2, label, p, button, a {
    font-family: 'Poppins';
  }
  a {
    font-weight: 600;
  }
  label {
    font-weight: 400;
  }
</style>
<body class="min-vh-100">
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a href="/" class="nav-item nav-link text-light">Home</a>
      <a href="/cidades" class="nav-item nav-link text-light">Cidades</a>
      <a href="/postos" class="nav-item nav-link text-light">Postos</a>
      <a href="/precos" class="nav-item nav-link text-light">Preços</a>
      <a href="/reports/precos" class="nav-item nav-link text-light">Relatórios de Preços</a>
      <a href="/reports/postos" class="nav-item nav-link text-light">Relatórios de Postos</a>
  </nav>
  <div class="container p-3 min-vh-80">
    <div class="title d-flex justify-content-center align-items-center">
      <h1 class="">@yield('title')</h1>
    </div>
    <section>
      @yield('content')
    </section>
  </div>
</body>
</html>
