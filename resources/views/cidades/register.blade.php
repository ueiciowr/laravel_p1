@extends('layouts.main')

@section('title')
  Cadastrar cursos
@endsection

@section('content')
  <form
    class="d-flex flex-column bd-highlight mb-3 justify-content-center align-items-center"
    action="{{route('cidades.store')}}"
    method="POST"
  >
    @csrf
    <div class="form-group w-25 mt-4 d-flex flex-column justify-content-center align-items-center">
      <label for="nome" class="mt-2">Nome da Cidade</label>
      <input type="text" name="nome" class="form-control" value="{{ old('nome') }}"  />

      <label for="uf" class="mt-2">Unidade Federativa (UF)</label>
      <input type="text" name="uf" class="form-control" value="{{ old('uf') }}" />

      <label label="cep" class="mt-2">CEP</label>
      <input type="text" name="cep" pattern="\d{0,5}-\d{0,3}"  class="form-control" value="{{ old('cep') }}" />

      <button type="submit" class="btn btn-primary pl-3 pr-3 mt-3">Enviar</button>
    </div>
  </form>
  @if ($errors->any())
    <div class="alert, alert-danger p-2">
      <strong>Não foi possível realizar cadastro, devido aos seguintes erros:</stro>
      <ul>
        @foreach ($errors->all() as $erro)
          <li>{{ $erro }}</li>
        @endforeach
      </ul>
    </div>
  @endif
@endsection
