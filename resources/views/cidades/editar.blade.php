@extends('layouts.main')

@section('title')
  Editar curso
@endsection

@section('content')
  <form
    class="d-flex flex-column bd-highlight mb-3 justify-content-center align-items-center"
    action="{{route('cidades.update', $cidade->id)}}"
    method="POST"
  >
    @csrf
    @method('PUT')
    <label for="nome">Nome da Cidade</label>
    <input type="text" name="nome" value="{{ $cidade->nome }}"  />

    <label for="uf">Unidade Federativa (UF)</label>
    <input type="text" name="uf" value="{{ $cidade->uf }}" />

    <label label="cep">CEP</label>
    <input type="text" name="cep" value="{{ $cidade->cep }}" />

    <button type="submit" class="btn btn-primary pl-3 pr-3 mt-3">Enviar</button>
  </form>
  @if ($errors->any())
    <div class="alert, alert-danger p-2">
      <strong>Não foi possível realizar cadastro, devido aos seguintes erros:</stro>
      <ul>
        @foreach ($errors->all() as $erro)
          <li>{{ $erro }}</li>
        @endforeach
      </ul>
    </div>
  @endif
@endsection
