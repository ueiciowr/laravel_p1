@extends('layouts.main')

<style>
  th, td {
    padding: .2rem 1rem;
  }
</style>

@section('title')
  Cidades
@endsection

@section('content')
  <section class="d-flex mt-4 justify-content-center align-center">
    <table class="table table-striped table-dark">
      <tr>
        <th>Id</th>
        <th>Nome</th>
        <th>UF</th>
        <th>CEP</th>
        <th>Editar</th>
        <th>Deletar</th>
      </tr>
      @foreach ($cidades as $cidade)
      <tr>
        <td>{{ $cidade->id }}</td>
        <td>{{ $cidade->nome }}</td>
        <td>{{ $cidade->uf }}</td>
        <td>{{ $cidade->cep }}</td>
        <td>
          <a
            href="{{url('cidades/'.$cidade->id.'/edit')}}"
            class="btn bg-primary border-none outline-none text-white"
          >
            Editar
          </a>
        </td>
        <td> 
          <form class="d-inline" action="{{ route('cidades.destroy',$cidade->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn bg-danger border-none outline-none text-white">Deletar</button>
          </form>
        </td>
      @endforeach
      </tr>
    </table>
  </section>
  <nav class="py-4 d-flex justify-content-center align-center">
    <a href="cidades/create">Cadastrar</a>
  </nav>
@endsection
