@extends('layouts.main')

@section('title')
  Cadastrar Postos
@endsection

@section('content')
  <form
    class="d-flex flex-column bd-highlight mb-3 justify-content-center align-items-center"
    action="{{route('postos.store')}}"
    method="POST"
  >
    @csrf
    <div class="form-group w-25 d-flex flex-column justify-content-center align-items-center">
      <br>
      <label for="nome">Nome do Posto</label>
      <input type="text" name="nome" class="form-control" value="{{ old('nome') }}"  />

      <label for="endereco">Endereço</label>
      <input type="text" name="endereco" class="form-control" value="{{ old('endereco') }}" />

      <label label="bairro">Bairro</label>
      <input type="text" name="bairro" class="form-control" value="{{ old('bairro') }}" />

      <label label="bandeira">Bandeira</label>
      <input type="text" name="bandeira" class="form-control" value="{{ old('bandeira') }}" />

      <label label="razao_social">Razão Social</label>
      <input type="text" name="razao_social" class="form-control" value="{{ old('razao_social') }}" />

      <label label="cnpj">CNPJ</label>
      <input type="text" name="cnpj" class="form-control" value="{{ old('cnpj') }}" />

      <select
        name="cidade_id"
        id="cidade_id"
        class="form-control  mt-4"
      >
        @foreach ($cidades as $cidade)
            <option name="cidade_id" value="{{ $cidade->id }}">{{ $cidade->nome }}</option>
        @endforeach
      </select>

      <button type="submit" class="btn btn-primary pl-3 pr-3 mt-3">Enviar</button>
    </div>
  </form>
  @if ($errors->any())
    <div class="alert, alert-danger p-2">
      <strong>Não foi possível realizar cadastro, devido aos seguintes erros:</stro>
      <ul>
        @foreach ($errors->all() as $erro)
          <li>{{ $erro }}</li>
        @endforeach
      </ul>
    </div>
  @endif
@endsection
