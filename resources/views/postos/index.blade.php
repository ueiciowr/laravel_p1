@extends('layouts.main')

<style>
  th, td {
    padding: .2rem 1rem;
  }
</style>

@section('title')
  Postos
@endsection

@section('content')
  <section class="d-flex mt-4 justify-content-center align-center">
    <table class="table table-striped table-dark">
      <tr>
        <th>Id</th>
        <th>Nome</th>
        <th>Endereço</th>
        <th>Bairro</th>
        <th>Bandeira</th>
        <th>Razao Social</th>
        <th>CNPJ</th>
        <th>Cidade</th>
        <th>Editar</th>
        <th>Deletar</th>
      </tr>
      @foreach ($postos as $posto)
      <tr>
        <td>{{ $posto->id }}</td>
        <td>{{ $posto->nome }}</td>
        <td>{{ $posto->endereco }}</td>
        <td>{{ $posto->bairro }}</td>
        <td>{{ $posto->bandeira }}</td>
        <td>{{ $posto->razao_social }}</td>
        <td>{{ $posto->cnpj }}</td>
        <td>{{ $posto->cidades->nome }}</td>
        <td>
          <a
            href="{{url('postos/'.$posto->id.'/edit')}}"
            class="btn bg-primary border-none outline-none text-white"
          >
            Editar
          </a>
        </td>
        <td> 
          <form class="d-inline" action="{{ route('postos.destroy',$posto->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn bg-danger border-none outline-none text-white">Deletar</button>
          </form>
        </td>
      @endforeach
      </tr>
    </table>
  </section>
  <nav class="py-4 d-flex justify-content-center align-center">
    <a href="postos/create">Cadastrar</a>
  </nav>
@endsection
