@extends('layouts.main')

@section('title')
  Cadastrar Preços
@endsection

@section('content')
  <form
    class="d-flex flex-column bd-highlight mb-3 justify-content-center align-items-center"
    action="{{route('precos.store')}}"
    method="POST"
  >
    @csrf
    <div class="form-group w-25 d-flex flex-column justify-content-center align-items-center">
      <br>
      <label for="tipo_combustivel">Combustível</label>
      <input type="text" name="tipo_combustivel" class="form-control" value="{{ old('tipo_combustivel') }}"  />

      <label for="data_coleta">Data de Coleta do Preço</label>
      <input type="text" name="data_coleta" class="form-control" value="{{ $result }}" />

      <label label="preco_venda">Preço de Venda</label>
      <input type="text" name="preco_venda" class="form-control" value="{{ old('preco_venda') }}" />

      <label label="postos_id" class="mt-4">Posto de Combustível</label>
      <select
        name="postos_id"
        id="postos_id"
        class="form-control"
      >
        @foreach ($postos as $posto)
            <option name="postos_id" value="{{ $posto->id }}">{{ $posto->nome  }} - ({{ $posto->cidades->nome }})</option>
        @endforeach
      </select>

      <button type="submit" class="btn btn-primary pl-3 pr-3 mt-3">Enviar</button>
    </div>
  </form>
  @if ($errors->any())
    <div class="alert, alert-danger p-2">
      <strong>Não foi possível realizar cadastro, devido aos seguintes erros:</stro>
      <ul>
        @foreach ($errors->all() as $erro)
          <li>{{ $erro }}</li>
        @endforeach
      </ul>
    </div>
  @endif
@endsection
