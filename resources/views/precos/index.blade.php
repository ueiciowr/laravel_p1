@extends('layouts.main')

<style>
  th, td {
    padding: .2rem 1rem;
  }
</style>

@section('title')
  Preços
@endsection

@section('content')
  <section class="d-flex mt-4 justify-content-center align-center">
    <table class="table table-striped table-dark">
      <tr>
        <th>Id</th>
        <th>Combustível</th>
        <th>Data de Coleta</th>
        <th>Preço de Venda</th>
        <th>Posto</th>
        <th>Editar</th>
        <th>Deletar</th>
      </tr>
     @if ($precos ?? '' != null)
        @foreach ($precos as $preco)
        <tr>
          <td>{{ $preco->id }}</td>
          <td>{{ $preco->tipo_combustivel }}</td>
          <td>{{ $preco->data_coleta }}</td>
          <td>{{ $preco->preco_venda }}</td>
          <td>{{ $preco->postos->nome }}</td>
          <td>
            <a
              href="{{url('precos/'.$preco->id.'/edit')}}"
              class="btn bg-primary border-none outline-none text-white"
            >
              Editar
            </a>
          </td>
          <td> 
            <form class="d-inline" action="{{ route('precos.destroy',$preco->id) }}" method="POST">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn bg-danger border-none outline-none text-white">Deletar</button>
            </form>
          </td>
        @endforeach
     @endif
      </tr>
    </table>
  </section>
  <nav class="py-4 d-flex justify-content-center align-center">
    <a href="precos/create">Cadastrar</a>
  </nav>
@endsection
