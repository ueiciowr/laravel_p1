@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="">
                    <nav class="navbar navbar-expand-lg navbar-dark bg-danger">
                        <a href="/login" class="nav-item nav-link text-light">Login</a>
                        <a href="/register" class="nav-item nav-link text-light">Registro</a>
                        <a href="/postos" class="nav-item nav-link text-light">Postos</a>
                        <a href="/cidades" class="nav-item nav-link text-light">Cidades</a>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
