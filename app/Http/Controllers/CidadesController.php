<?php

namespace App\Http\Controllers;

use App\Models\Cidades;
use Illuminate\Http\Request;

class CidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cidades = Cidades::all();
        return view('cidades/index', compact('cidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cidades/register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cidades::paramsValidate($request);

        Cidades::create($request->all());
        return redirect('cidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cidades  $cidades
     * @return \Illuminate\Http\Response
     */
    public function show(Cidades $cidades)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cidades  $cidades
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cidade = Cidades::find($id);
        return view('cidades/editar', compact('cidade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cidades  $cidades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cidades::paramsValidate($request);

        $cidade = Cidades::find($id);
        $cidade->update($request->all());
        return redirect('cidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cidades  $cidades
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cidades $cidades, $id)
    {
        $cidade = Cidades::find($id);
        $cidade->delete();
        return redirect('cidades');
    }

    public function postos(Request $request){
        $cidades = Cidades::all();
        $selectedCity = Cidades::find($request->id);
        if(isset($selectedCity->postos)){
            $postos = $selectedCity->postos;
        } else {
            $postos = null;
        }
        return view('reports/postos', compact('cidades', 'postos'));
    }
}