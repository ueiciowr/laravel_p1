<?php

namespace App\Http\Controllers;

use App\Models\Postos;
use App\Models\Precos;
use Illuminate\Http\Request;
use DateTime;

class PrecosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $precos = Precos::all();
        return view('precos/index', compact('precos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $postos = Postos::all();
        $now = new DateTime();
        $result = $now->format('Y-m-d H:i:s');
        return view('precos/register', compact('postos', 'result'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Precos::paramsValidate($request);

        Precos::create($request->all());
        return redirect('precos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Precos  $precos
     * @return \Illuminate\Http\Response
     */
    public function show(Precos $precos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Precos  $precos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $precos = Precos::find($id);
        $postos = Postos::all();
        return view('precos/editar', compact('precos', 'postos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Precos  $precos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Precos::paramsValidate($request);

        $preco = Precos::find($id);
        $preco->update($request->all());
        return redirect('precos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Precos  $precos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $preco = Precos::find($id);
        $preco->delete();
        return redirect('precos');
    }
}