<?php

namespace App\Http\Controllers;

use App\Models\Cidades;
use App\Models\Postos;
use Illuminate\Http\Request;

class PostosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postos = Postos::all();
        return view('postos/index', compact('postos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cidades = Cidades::all();
        return view('postos/register', compact('cidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Postos::paramsValidate($request);
        Postos::create($request->all());
        return redirect('postos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Postos  $postos
     * @return \Illuminate\Http\Response
     */
    public function show(Postos $postos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Postos  $postos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postos = Postos::find($id);
        $cidades = Cidades::all();
        return view('postos/editar', compact('postos', 'cidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Postos  $postos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Postos::paramsValidate($request);

        $posto = Postos::find($id);
        $posto->update($request->all());
        return redirect('postos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Postos  $postos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $posto = Postos::find($id);
        $posto->delete();
        return redirect('postos');
    }

    public function precos(Request $request){
        $postos = Postos::all();
        $selectedPosto = Postos::find($request->id);
        if(isset($selectedPosto->preco_postos)){
            $preco_postos = $selectedPosto->preco_postos;
        } else {
            $preco_postos = null;
        }
        return view('reports/precos', compact('postos', 'preco_postos'));
    }
}