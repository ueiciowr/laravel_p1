<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Postos;
use Illuminate\Http\Request;

class Cidades extends Model
{
    protected $table = 'cidades';
    protected $fillable = ['id', 'nome', 'uf', 'cep'];

    public function postos() {
        return $this->hasMany(Postos::class, 'cidade_id');
    }

    public static function paramsValidate(Request $request)
    {
        $rules = [
            'nome' => 'required|min:5|max:100',
            'uf' => 'required|min:2',
            'cep' => 'required',
        ];

        $request->validate($rules);
    }
    use HasFactory;
}