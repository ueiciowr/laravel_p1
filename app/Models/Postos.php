<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cidades;
use App\Models\Precos;
use Illuminate\Http\Request;

class Postos extends Model
{
    protected $table = 'postos';
    protected $fillable = [
        'id',
        'nome',
        'endereco',
        'bairro',
        'bandeira',
        'razao_social',
        'cnpj',
        'cidade_id'
    ];

    public function cidades() {
        return $this->belongsTo(Cidades::class, 'cidade_id');
    }

    public function preco_postos() {
        return $this->hasMany(Precos::class);
    }

    static function paramsValidate(Request $request)
    {
        $rules = [
            'nome' => 'required|min:5|max:100',
            'endereco' => 'required|min:5|max:500',
            'bandeira' => 'required|min:2|max:100',
            'bairro' => 'required|min:5|max:100',
            'razao_social' => 'required|min:2|max:150',
            'cnpj' => 'required|min:14'
        ];

        $request->validate($rules);
    }
    use HasFactory;
}