<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Postos;
use Illuminate\Http\Request;

class Precos extends Model
{
    protected $table = 'precos';
    protected $fillable = [
        'id',
        'tipo_combustivel',
        'data_coleta',
        'preco_venda',
        'postos_id',
    ];
    
    public function postos() {
        return $this->belongsTo(Postos::class, 'postos_id');
    }

    static function paramsValidate(Request $request)
    {
        $rules = [
            'tipo_combustivel' => 'required|min:5|max:100',
            'data_coleta' => 'required',
            'preco_venda' => 'required|min:2|max:10',
            'postos_id' => 'required',
        ];

        $request->validate($rules);
    }
    use HasFactory;
}